#!/bin/sh

docker rm -f typo3 db-typo3-example
docker volume prune --force --filter 'label=group=typo3'
docker-compose up -d typo3 db-typo3-example