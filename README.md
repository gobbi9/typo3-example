# Typo 3 Example Project

## Starting it

1. Install docker and docker-compose
2. `docker-compose up -d`, try again with sudo or similar if it does not work
3. Open TYPO3 at <http://localhost:1080> and setup as in the images below

### Step 2

Password=`root` as setup in the docker-compose.yml file.

![Step 2](typo3-setup-2.png)

### Step 3

![Step 3](typo3-setup-3.png)

### Step 4

![Step 4](typo3-setup-4.png)

### Step 5

![Step 5](typo3-setup-5.png)

## Services that should be running

1. MySQL 5.7 database at localhost:3306, user: `root` with password: `root`. Example database called **classicmodels**, taken from [MySQLTutorial](http://www.mysqltutorial.org/mysql-sample-database.aspx). The ER Model of this example database is also in the link. TYPO3 Database is called **typo3_example**
2. [Adminer](https://www.adminer.org/) 4.7 at <http://localhost:8080>. Log in with System=MySQL, Server=`db-typo3-example`, Username=`root`, Password=`root`, Database=`classicmodels`
3. Login in TYPO3 at <http://localhost:1080/typo3/index.php> with Username=`admin` and Password=`uguwVV335zNtNYL`
4. Traefik Dashboard (Reverse Proxy Server) at <http://localhost:2080>
5. Tracer UI (Jaeger) at <http://localhost:16686>

### Local domain names for the containers (tested only on Linux)

* Adminer: <http://adminer.localhost>
* Typo3: <http://typo3.localhost/typo3/index.php>
* Traefik Dashboard: <http://traefik.localhost>
* Jaeger UI: <http://tracer.localhost>

## How to reset Typo3

This command will delete ALL databases in the MySQL instance and also Typo3 and its volumes:

```bash
docker rm -f typo3 db-typo3-example
docker volume prune --force --filter 'label=group=typo3'
```

To start it again, issue:

```bash
docker-compose up -d typo3 db-typo3-example
```

Or (if on Linux) the following shell script can be used:

```bash
./reset-typo3.sh
```

## Deleting the whole docker stack

This command will also delete the volumes and configuration:

```bash
docker-compose down --rmi local --volumes
```

## Updating image versions

During development it's possible that the image tags from the docker images are no longer up to date. To update them, execute: `docker-compose pull`.

## Updating container configuration

After changing the version of any command line configuration of the containers, it's necessary to recreate it:

```bash
docker-compose up -d CONTAINER_NAME
```

> CAREFUL: when changing the typo3 version the database must also be deleted, therefore it's recommended to follow the steps mentioned above at _How to reset Typo3_
